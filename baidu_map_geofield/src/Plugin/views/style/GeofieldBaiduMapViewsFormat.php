<?php
namespace Drupal\baidu_map_geofield\Plugin\views\style;

use Drupal\core\form\FormStateInterface;
use Drupal\geofield\GeoPHP\GeoPHPInterface;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\views\Plugin\views\style\StylePluginBase;
use Drupal\baidu_map_geofield\GeofieldBaiduMapFieldTrait;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Style plugin to render a list of years and months
 * in reverse chronological order linked to content.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "baidu_map_geofield",
 *   title = @Translation("Baidu Map"),
 *   help = @Translation("baidu map geofield format."),
 *   theme = "baidu_map_geofield",
 *   display_types = { "normal" }
 * )
 */
class GeofieldBaiduMapViewsFormat extends StylePluginBase {
  use GeofieldBaiduMapFieldTrait;

  /**
   * The geoPhpWrapper service.
   *
   * @var \Drupal\geofield\GeoPHP\GeoPHPInterface
   */
  protected $geoPhpWrapper;

  /**
   * GeofieldBaiduMapViewsFormat constructor.
   *
   * @param array                                   $configuration
   * @param string                                  $plugin_id
   * @param mixed                                   $plugin_definition
   * @param \Drupal\geofield\GeoPHP\GeoPHPInterface $geophp_wrapper
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, GeoPHPInterface $geophp_wrapper) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->geoPhpWrapper = $geophp_wrapper;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array                                                     $configuration
   * @param string                                                    $plugin_id
   * @param mixed                                                     $plugin_definition
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('geofield.geophp')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['data_source'] = ['default' => ''];
    $options['popup_source'] = ['default' => ''];
    $options['alt_text'] = ['default' => ''];
    // See: baidu_map_geofield_field_formatter_info.
    $options['map_dimensions'] = [
      'default' => [
        'width' => '100%',
        'height' => '400px'
      ]
    ];
    $options['baidu_map_geofield_zoom'] = ['default' => 'auto'];
    $options['baidu_map_geofield_type'] = ['default' => 'normal'];
    $options['baidu_map_geofield_style'] = ['default' => 'normal'];
    $options['baidu_map_geofield_showtraffic'] = ['default' => FALSE];
    $options['baidu_map_geofield_navigationcontrol'] = ['default' => ''];
    $options['baidu_map_geofield_scrollwheel'] = ['default' => FALSE];
    $options['baidu_map_geofield_draggable'] = ['default' => TRUE];
    $options['baidu_map_geofield_maptypecontrol'] = ['default' => FALSE];
    $options['baidu_map_geofield_scalecontrol'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    if (!$this->usesFields() && $this->usesGrouping()) {
      $form['error'] = array(
        '#markup' => t('Please add at least 1 geofield to the view'),
      );
    } else {
      $options = ['' => $this->t('- None -')];
      $field_labels = $this->displayHandler->getFieldLabels(TRUE);
      $options += $field_labels;

      $form['data_source'] = array(
        '#type' => 'select',
        '#title' => t('Data Source'),
        '#description' => t('Which field contains geodata?'),
        '#options' => $options,
        '#default_value' => $this->options['data_source'] ? $this->options['data_source'] : '',
      );

      $form['popup_source'] = array(
        '#type' => 'select',
        '#title' => t('Popup Text'),
        '#options' => $options,
        '#default_value' => $this->options['popup_source'] ? $this->options['popup_source'] : '',
      );

      $form['alt_text'] = array(
        '#type' => 'textarea',
        '#title' => t('Alternate Text'),
        '#description' => t('This text shows up when a user does not have javascript enabled'),
        '#default_value' => $this->options['alt_text'] ? $this->options['alt_text'] : '',
      );

      // set default form.
      $defaultSettings = $this->getDefaultSettings();
      $this->setMapDimensionsElement($this->options, $form);
      $this->setMapStyleElement($this->options, $defaultSettings, $form);
    }
  }

  /**
   * render map.
   *
   *
   * @return array
   */
  public function render() {
    $values = $this->view->result;
    $data = [];
    $souceField = $this->options['data_source'];
    $popAlt = $this->options['popup_source'];
    foreach($values as $index => $value) {
      if (!$value) {
        continue;
      }
      $geometry = $this->geoPhpWrapper->load($this->getFieldValue($index, $souceField));
      if (!$geometry) {
        continue;
      }
      $datum =  json_decode($geometry->out('json'));
      if ($this->getFieldValue($index, $popAlt)) {
        $description = $this->getField($index, $popAlt)->__toString();
      }
      $datum->properties = array(
        'description' => $description ?? '',
      );
      $data[] = $datum;
    }
    $data = array(
      'type' => 'GeometryCollection',
      'geometries' => $data,
    );
    // html container id.
    $container_id = $this->view->id() . '_' . $this->view->current_display;

    $js_settings = array(
      $container_id => array(
        'map_id' => $container_id,
        'map_settings' => $this->options,
        'data' => $data,
      ),
    );
    $width = $this->options['map_dimensions']['width'];
    $height = $this->options['map_dimensions']['height'];
    $element = new FormattableMarkup('<div style="width: @width; height: @height" id="@container_id" class="geofield_baidu_map">@alt_text</div>', array(
      '@width' => $width,
      '@height' => $height,
      '@container_id' => $container_id,
      '@alt_text' => $this->options['alt_text'],
    ));

    $display = [
      '#type' => 'markup',
      '#markup' => $element,
    ];
    $display['#attached']['library'][] = 'baidu_map_geofield/baidu_map_geofield';
    $display['#attached']['library'][] = 'baidu_map_geofield/geojson';

    $display['#attached']['drupalSettings'] = [
      'baidu_map_geofield' => $js_settings,
    ];
    return $display;
  }
}
