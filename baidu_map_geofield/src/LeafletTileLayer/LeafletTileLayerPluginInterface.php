<?php

namespace Drupal\baidu_map_geofield\leafletTileLayers;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Leaflet tile layers plugin plugins.
 */
interface LeafletTileLayerPluginInterface extends PluginInspectionInterface {


  // Add get/set methods for your plugin type here.

}
