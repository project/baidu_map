<?php

namespace Drupal\baidu_map_geofield\Controller;

use Drupal\Core\Url;
use GuzzleHttp\Client;
use Drupal\Core\Controller\ControllerBase;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Baidu map controller.
 */
class BMapController extends ControllerBase {

  /**
   * Guzzle\Client instance.
   *
   * @var \Guzzle\Client
   */
  protected $httpClient;

  /**
   * {@inheritdoc}
   */
  public function __construct(Client $http_client) {
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client')
    );
  }

  /**
   * get geocoder.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return array|\Symfony\Component\HttpFoundation\JsonResponse
   */
  public function getPlace(Request $request) {
    $config = \Drupal::config('baidu_map.settings');

    $query = array(
      'query' => $request->query->get('q'),
      'region' => '全国',
      'ak' => $config->get('baidu_map_api_place_search_key'),
      'output' => 'json',
      'city_limit' => 10
    );

    $url = Url::fromUri($config->get('baidu_map_api_place_search_url'), [
      'query' => $query
    ]);

    $response = [];

    try {
      $request = $this->httpClient->get($url->toString(), [
        'headers' => ['Accept' => 'application/json']
      ]);

      $request_body = (string) $request->getBody();

      if ($request->getStatusCode() === 200 && !empty($request_body)) {
        foreach(json_decode($request_body, TRUE)['result'] as $data) {
          $response[] = [
            'label' => $data['name'],
            'value' => $data
          ];
        }
      }
    }
    catch (RequestException $e) {
      return $response;
    }

    return new JsonResponse($response);
  }
}
